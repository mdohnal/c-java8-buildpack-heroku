Heroku buildpack: Java
=========================

This is a [Heroku buildpack](http://devcenter.heroku.com/articles/buildpack) for Java apps.

Cloned from [https://github.com/heroku/heroku-buildpack-java](https://github.com/heroku/heroku-buildpack-java) and modified for standalone JAR deployment.

Usage
-----

Choose a JDK
--------------
Create a `system.properties` file in the root of your project directory and set `java.runtime.version=1.7`.

Example:

    $ ls
    Procfile pom.xml src

    $ echo "java.runtime.version=1.7" > system.properties

    $ git add system.properties && git commit -m "Java 7"

    $ git push heroku master
    ...
    -----> Heroku receiving push
    -----> Fetching custom language pack... done
    -----> Java app detected
    -----> Installing OpenJDK 1.7... done
    ...

License
-------

Licensed under the MIT License. See LICENSE file.
